FROM rabbitmq:3.7-management

ENV RABBITMQ_LOGS=/tmp/lograbbitmq RABBITMQ_SASL_LOGS=/tmp/lograbbitmq
COPY container_files/usr-local-bin/ /usr/local/bin/
ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["rabbitmq-server"]